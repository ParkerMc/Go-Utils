# Go-Utils

## Development:
Anyone is welcome to contribute to the project but pull requests will be accepted based on how useful they are.

Any commit must pass the following:
* [golint](https://github.com/golang/lint)
* [go vet](https://golang.org/cmd/vet/)
* [gofmt](https://golang.org/cmd/gofmt)
* [go test](https://golang.org/cmd/go/#hdr-Test_packages)

## Install

`go get gitlab.com/ParkerMc/Go-Utils`

## Usage

##### configreader
Used to read configs to structs including nested structs
