package configreader

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
)

const folderPerms os.FileMode = os.FileMode(0755) // Set the folder perms so the owner can do anything and other uses can only see
const filePerms os.FileMode = os.FileMode(0600)   // Set the file perms so only the owner can see and owner can also write

// LoadConfig loads the config returns if file was loaded and error
func LoadConfig(config interface{}, configPath string) (bool, error) {
	if _, err := os.Stat(configPath); !os.IsNotExist(err) { // If the file exits
		jsonBytes, err := ioutil.ReadFile(configPath) // Read the file
		if err != nil {
			return true, err
		}
		err = json.Unmarshal(jsonBytes, config) // un marshal the data into the struct
		if err != nil {
			return true, err
		}
		return true, nil
	}

	return false, nil
}

// SaveConfig saves the config
func SaveConfig(config interface{}, configPath string) error {
	configDirectory := filepath.Dir(configPath) // Get the folder the config is in

	if _, err := os.Stat(configDirectory); os.IsNotExist(err) { // If the folder doesn't exits create it
		err = os.MkdirAll(configDirectory, folderPerms)
		if err != nil {
			return err
		}
	}

	jsonBytes, err := json.MarshalIndent(config, "", "\t") // Marshal struct to bytes
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(configPath, jsonBytes, filePerms) // Write bytes to file
	return err
}
